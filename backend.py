from flask import Flask, jsonify, request
from flask_restful import Resource, Api
from flask_pymongo import PyMongo
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_claims
)
from auth.json_validation import validate_login
from flask_cors import CORS
from hashids import Hashids
from roots.user import User
from roots.user import UserAdd
from roots.user import UserScore
from roots.users import Users
from roots.address import Address
from roots.symptom import Symptom
from roots.symptoms import Symptoms
from roots.symptom import SymptomAdd
from roots.profile import Profile
from roots.insurance import Insurance
from roots.user_symptoms import UserAddSymptoms, UserDelSymptoms
from roots.custom_jwt_annotations import hashids

app = Flask(__name__)
CORS(app)
api = Api(app)
app.config["MONGO_URI"] = "mongodb://hackaton:" \
    "wDfNUnK5AZxqM7d4@ds111279.mlab.com:11279/stubenhacker" \
    "?retryWrites=false"
mongo = PyMongo(app)

app.config["JWT_SECRET_KEY"] = "correcthorsebatterystaple"
jwt = JWTManager(app)

api.add_resource(User, '/users/<string:user_id>',
                 resource_class_kwargs={'mongo': mongo})

api.add_resource(UserAdd, '/users/add', resource_class_kwargs={'mongo': mongo})
api.add_resource(Users, '/users', resource_class_kwargs={'mongo': mongo})


api.add_resource(
    Address,
    '/users/<string:user_id>/address',
    resource_class_kwargs={'mongo': mongo})

api.add_resource(
    Profile,
    '/users/<string:user_id>/profile',
    resource_class_kwargs={'mongo': mongo})

api.add_resource(
    Insurance,
    '/users/<string:user_id>/insurance',
    resource_class_kwargs={'mongo': mongo})

# TODO Write symptome-user relation

# api.add_resource(
#        Symptome,
#        '/symptoms/<string:symptom_id>',
#        resource_class_kwargs={'mongo': mongo})

api.add_resource(
    Symptoms,
    '/symptoms',
    resource_class_kwargs={'mongo': mongo})

api.add_resource(
    SymptomAdd,
    '/symptoms/add',
    resource_class_kwargs={'mongo': mongo})

api.add_resource(
    UserAddSymptoms,
    '/users/<string:user_id>/symptoms/extend',
    resource_class_kwargs={'mongo': mongo})

api.add_resource(
    UserDelSymptoms,
    '/users/<string:user_id>/symptoms/remove',
    resource_class_kwargs={'mongo': mongo})

api.add_resource(
    UserScore,
    "/users/<string:user_id>/score",
    resource_class_kwargs={'mongo': mongo})


@jwt.user_claims_loader
def add_claims_to_user(identity):
    accesses = []

    query = {"username": identity}
    if mongo.db.login.find_one(query) is not None:
        accesses.append('user')

    if mongo.db.backoffice_login.find_one(query) is not None:
        accesses.append('backoffice')

    return {
        'identity': identity,
        'accesses': accesses
    }


@app.route('/login', methods=['POST'])
def userLogin():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    loginData = request.json

    if not validate_login(loginData):
        return jsonify({"msg": "Login data is incomplete"}), 400

    query = {"username": loginData['username']}
    user = mongo.db.login.find_one(query)

    if user is None:
        return jsonify({"msg": "Login data is invalid"}), 401

    username = loginData['username']
    uuid = hex(hashids.decode(username)[0])[2:]
    return jsonify({'user_id': uuid,
                    'access_token': create_access_token(username)}), 200


@app.route('/backoffice/login', methods=['POST'])
def backofficeLogin():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    loginData = request.json

    if not validate_login(loginData):
        return jsonify({"msg": "Login data is incomplete"}), 400

    query = {"username": loginData['username']}
    user = mongo.db.backoffice_login.find_one(query)

    if user is None:
        return jsonify({"msg": "Login data is invalid"}), 401

    username = loginData['username']
    return jsonify({'access_token': create_access_token(username)}), 200


if __name__ == '__main__':
    app.run()
