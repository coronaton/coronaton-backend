import json
from jsonschema import validate


def validate_user(user_data):
    with open('../schema/user_schema_python.json') as f:
        schema = json.load(f)
    try:
        validate(user_data, schema=schema)
    except Exception:
        return False
    else:
        return True


def validate_login(login_data):
    with open('schema/login_schema.json') as f:
        schema = json.load(f)
    try:
        validate(login_data, schema=schema)
    except Exception:
        return False
    else:
        return True
