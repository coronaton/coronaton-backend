import time
import json
import sys

from flask import jsonify
from flask_restful import Resource, request
from collections import OrderedDict
from roots.custom_jwt_annotations import backoffice_required, hashids


class Users(Resource):
    def __init__(self, **kwargs):
        self.mongo = kwargs['mongo']
        if "users" not in self.mongo.db.collection_names():
            self.mongo.db.create_collection("users")
        with open('schema/user_schema.json', 'r') as schema_file:
            schema = json.load(schema_file)
            cmd = OrderedDict([('collMod', 'users'),
                               ('validator', schema),
                               ('validationLevel', 'moderate')])
            self.mongo.db.command(cmd)

    @backoffice_required
    def get(self):
        users = list(self.mongo.db.users.find({}))
        for user in users:
            user["_id"] = str(user["_id"])
        return users, 200

    def post(self):
        content = request.get_json()
        content['created'] = time.time()
        content['lastUpdated'] = content['created']

        try:
            self.mongo.db.users.insert_one(content)
        except Exception:
            return "Bad Request", 400

        username = hashids.encode(int(str(content['_id']), 16))
        password = content['birthdate']
        loginCredential = {"username": username, "password": password}
        self.mongo.db.login.insert_one(loginCredential)

        content['_id'] = str(content["_id"])

        return jsonify({"username": username, "user": content})
