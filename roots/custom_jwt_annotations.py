from functools import wraps
from flask import jsonify
from flask_jwt_extended import (
    verify_jwt_in_request, get_jwt_claims
)
from hashids import Hashids

hashids = Hashids()


def user_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if 'user' in claims['accesses'] or 'backoffice' in claims['accesses']:
            return fn(*args, **kwargs)
        else:
            return {"msg": "Users only!"}, 403
    return wrapper


def backoffice_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if 'backoffice' in claims['accesses']:
            return fn(*args, **kwargs)
        else:
            return {"msg": "Backoffice only!"}, 403
    return wrapper


def owns_user_data_required(name_of_id):
    def decorator(fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            verify_jwt_in_request()
            claims = get_jwt_claims()
            username = hashids.encode(int(str(kwargs[name_of_id]), 16))
            if (claims['identity'] == username or
                    'backoffice' in claims['accesses']):
                return fn(*args, **kwargs)
            else:
                return {"msg": "Owner only!"}, 403
        return wrapper
    return decorator
