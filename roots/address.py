import time
from flask_restful import Resource, request
from bson.objectid import ObjectId
from roots.custom_jwt_annotations import owns_user_data_required


class Address(Resource):
    def __init__(self, **kwargs):
        self.mongo = kwargs['mongo']

    @owns_user_data_required(name_of_id='user_id')
    def put(self, user_id):
        object_id = ObjectId(user_id)
        content = {'address': request.get_json()}
        content['lastUpdated'] = time.time()
        val = self.mongo.db.users.update_one(
                {'_id': object_id},
                {'$set': content})
        if val.matched_count:
            return content
        else:
            return None, 404
