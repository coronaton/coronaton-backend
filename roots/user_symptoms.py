import time
from flask_restful import Resource, request
from bson.objectid import ObjectId
from roots.custom_jwt_annotations import owns_user_data_required


class UserDelSymptoms(Resource):
    def __init__(self, **kwargs):
        self.mongo = kwargs['mongo']

    @owns_user_data_required(name_of_id='user_id')
    def put(self, user_id):
        object_id = ObjectId(user_id)
        to_delet = request.get_json()
        query = {"_id": object_id}
        user = self.mongo.db.users.find_one_or_404(query)

        try:
            elements = user['profile']['symptoms']
        except Exception:
            return None, 404

        symptoms = [symptom for symptom in elements if symptom not in to_delet]
        content['lastUpdated'] = time.time()
        val = self.mongo.db.users.update_one(
                {'_id': object_id},
                {'$set': {'profile': {'symptoms': symptoms}}})
        if val.matched_count:
            return content
        else:
            return None, 404


class UserAddSymptoms(Resource):
    def __init__(self, **kwargs):
        self.mongo = kwargs['mongo']

    @owns_user_data_required(name_of_id='user_id')
    def post(self, user_id):
        query = {"_id": ObjectId(user_id)}
        content = self.mongo.db.users.find_one_or_404(query)
        json_content = request.get_json()

        if 'symptoms' not in content['profile'].keys():
            content['profile']['symptoms'] = []

        for item in json_content:
            if item not in content['profile']['symptoms']:
                content['profile']['symptoms'].append(item)

        db_update = {
                'profile': {
                    'symptoms': content['profile']['symptoms']
                    }
                }
        val = self.mongo.db.users.update_one(
                {'_id': ObjectId(user_id)},
                {'$set': db_update})
        if val.matched_count:
            return db_update
        else:
            return None, 404
