from flask_restful import Resource, request
from roots.custom_jwt_annotations import user_required


class Symptoms(Resource):
    def __init__(self, **kwargs):
        self.mongo = kwargs['mongo']

    @user_required
    def get(self):
        symptoms = list(self.mongo.db.symptoms.find({}))
        for symptom in symptoms:
            symptom['_id'] = str(symptom['_id'])
        return symptoms
