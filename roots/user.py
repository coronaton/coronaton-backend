import time
from flask_restful import Resource, request
from bson.objectid import ObjectId
from roots.custom_jwt_annotations import (
    owns_user_data_required, backoffice_required
)


class User(Resource):
    def __init__(self, **kwargs):
        self.mongo = kwargs['mongo']

    @owns_user_data_required(name_of_id='user_id')
    def get(self, user_id):
        query = {"_id": ObjectId(user_id)}
        user = self.mongo.db.users.find_one_or_404(query)
        if user is not None:
            user["_id"] = str(user["_id"])
            return user
        return None, 404

    @owns_user_data_required(name_of_id='user_id')
    def put(self, user_id):
        object_id = ObjectId(user_id)
        content = request.get_json()
        content['lastUpdated'] = time.time()
        val = self.mongo.db.users.update_one(
            {'_id': object_id}, {'$set': content})
        if val.matched_count:
            return self.get(user_id)
        else:
            return None, 404

    @backoffice_required
    def delete(self, user_id):
        query = {"_id": ObjectId(user_id)}
        self.mongo.db.users.delete_many(query)
        # todo fix delete not deleting docs
        return None


class UserAdd(Resource):
    def __init__(self, **kwargs):
        self.mongo = kwargs['mongo']

    def post(self):
        content = request.get_json()
        print(len(content['profile']['symptoms']))
        if len(content['profile']['symptoms']) < 2:
            return {'message': "#staythefuckathome"}, 200

        content['created'] = time.time()
        content['lastUpdated'] = content['created']

        try:
            self.mongo.db.users.insert_one(content)
        except Exception:
            return "Bad Request", 400

        username = hashids.encode(int(str(content['_id']), 16))
        password = content['birthdate']
        loginCredential = {"username": username, "password": password}
        self.mongo.db.login.insert_one(loginCredential)

        return {"username": username}


class UserScore(Resource):
    def __init__(self, **kwargs):
        self.mongo = kwargs['mongo']

    @owns_user_data_required(name_of_id='user_id')
    def get(self, user_id):
        query = {"_id": ObjectId(user_id)}
        user = self.mongo.db.users.find_one_or_404(query)
        if user is not None:
            user["_id"] = str(user["_id"])
            try:
                score = len(user['profile']['symptoms'])
                return {"score": score}, 200
            except Exception:
                return {"score": 0}, 200
        return None, 404
