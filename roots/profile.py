import time
from flask_restful import Resource, request
from bson.objectid import ObjectId
from roots.custom_jwt_annotations import owns_user_data_required


class Profile(Resource):
    def __init__(self, **kwargs):
        self.mongo = kwargs['mongo']

    @owns_user_data_required(name_of_id='user_id')
    def get(self, user_id):
        query = {"_id": ObjectId(user_id)}
        user = self.mongo.db.users.find_one_or_404(query)
        if user is not None:
            user["_id"] = str(user["_id"])
            try:
                return user['profile']
            except Exception:
                return None, 400
        else:
            return None, 404

    @owns_user_data_required(name_of_id='user_id')
    def put(self, user_id):
        object_id = ObjectId(user_id)
        content = {'profile': request.get_json()}
        content['lastUpdated'] = time.time()
        val = self.mongo.db.users.update_one(
                {'_id': object_id}, {'$set': content})
        if val.matched_count:
            return content
        else:
            return None, 404
