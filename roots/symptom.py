import time
from flask_restful import Resource, request
from roots.custom_jwt_annotations import backoffice_required


class Symptom(Resource):
    def __init__(self, **kwargs):
        self.mongo = kwargs['mongo']

    def get(self, symptom_id):
        pass

    def put(self, symptom_id):
        pass

    def delete(self, symptom_id):
        pass


class SymptomAdd(Resource):
    def __init__(self, **kwargs):
        self.mongo = kwargs['mongo']

    @backoffice_required
    def post(self):
        content = request.get_json()
        print(content)
        self.mongo.db.symptoms.insert_one(content)
        content['_id'] = str(content['_id'])
        return content
