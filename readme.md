# The Backend
This is the Coronaton backend documentation.
It provides a restlike api for the Coronaton test cordination.
The data is stored in a mongo DB whereas the comunication is done with python3/flask.

# Requirements

The following software must be installed:

## Debian like distributions

* python3
* python3-pip

## Arch like distributions

* python
* python-pip

## Python dependencies

Clone or download the repository, change your path working directory into it.

And run:

`pip install --user -r requirements.txt`

# Execution
## Debian like distributions

`python3 backend.py`

## Arch like distributions

`python backend.py`

## Docker

Comming soon.

# Api Reference
## End Points
* **/users**
    * GET: list of all users
* **/users/add**
    * POST: add a user returns user object if user should be tested or message if user is not prioritized for test
* **/users/user\_id**
    * GET: list all data of the user
    * PUT: update information about the user
    * DELETE: remove the user
* **/users/user\_id/address**
    * PUT: update the address information
* **/users/user\_id/profile**
    * GET: Show the informations from the profile
    * PUT: Update the profile
* **/users/user\_id/insurance**
    * PUT: Update insuarnce values
* **/users/user\_id/score**
    * GET: get users score  
**Symptoms is somehow outdated**

* **/symptoms**
    * Foo:
* **/symptoms/add**
    * POST: add a symptom
* **/users/user\_id/symptoms/extend**
    * POST: add a list of symptoms to a user
* **/users/user\_id/symptoms/remove**
    * PUT: remove a list of symptoms from a user
## Json Schemes
```json
UserData {
    forename: string,

    surname: string,
     birthdate: string,
     address?:{         street: string,
         zipcode: string,
     },
     email?: string,
     phone?: string,
     profile: {
         symptoms: [string/liste],
         mobility?: string,
         insurance?: Insurance 
         contact: { 
            suspected: number,
            confirmed: number
         },
         job:{
            exposure: number,
            importance: number,
         },
         risks?: [string],
         medication?: [string],
         traveling?: {
            country: string,
            region: string
         },
         flatmates: number
     },
     mobility?: 'FOOT' | 'VEHICLE' | 'PUBLIC_TRANSPORT'
}
```

```json
Score{
    "score":int
}
```

```json
message{
    "message":string
}
```
## Authentification

Log in as user using POST `{username: string, password: string}` via `\login`
username is the assigned username after user creation
password is the used birthdate at user creation (format "YYYY-MM-DD")
returns `{user_id: string, access_token: bearer token}`

Log in as backoffice using POST `{username: string, password: string}` via `\backoffice\login`
currently no signup for a backoffice user
returns `{access_token: bearer token}`

### End point access protection
* **/users**
    * GET: backoffice
* **/users/add**
    * POST: none
* **/users/user\_id**
    * GET: backoffice or owning user
    * PUT: backoffice or owning user
    * DELETE: backoffice
* **/users/user\_id/address**
    * PUT: backoffice or owning user
* **/users/user\_id/profile**
    * GET: backoffice or owning user
    * PUT: backoffice or owning user
* **/users/user\_id/insurance**
    * PUT: backoffice or owning user
* **/users/user\_id/score**
    * GET: backoffice or owning user
**Symptoms is somehow outdated**

* **/symptoms**
    * GET: User
* **/symptoms/add**
    * POST: backoffice
* **/users/user\_id/symptoms/extend**
    * POST: backoffice or owning user
* **/users/user\_id/symptoms/remove**
    * PUT: backoffice or owning user
